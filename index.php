<?php 
	
	$pdo = new PDO('mysql:host=localhost;dbname=imbdb_movies;charset=utf8', 'root', 'root'); // susijungia su DB

	$query = $pdo->prepare("SELECT * FROM `movies`"); // Paruosia SQL uzkalusa
	$query->execute(); // Ivykdo paruosta SQL uzklausa

	$movies = $query->fetchAll(PDO::FETCH_ASSOC); // Grazina duomenis asociatyviniu masyvu

	// echo "<pre>";
	// print_r($movies);
	// echo "</pre>";

?>


<?php include 'templates/header.view.php' ?>
<?php include 'templates/menu.view.php' ?>

<main class="container">
	<h2>Movies</h2>
	<div class="row">
		<div class="col-lg-12">
		   <div class="input-group">
		   		<span class="input-group-addon" id="basic-addon1">Search</span>
		     	<input type="text" class="form-control" placeholder="Enter movie name...">
		     	<span class="input-group-btn">
		       		<button class="btn btn-primary" type="button">Go!</button>
		     	</span>
		   </div><!-- /input-group -->
		 </div><!-- /.col-lg-6 -->
	</div>
	<div class="row thumb">

		<?php foreach($movies as $movie): ?>
    	<div class="col-md-3 text-center">
    	    <div class="caption ">
    	        <h4><?= $movie['title']; ?></h4> 
    	    </div>
    	    <div class="thumbnail">
    	        <img src="<?= $movies['image']; ?>" alt="...">
    	    </div>
    	    <span>Metai:</span>
    	    <span>Trukme:</span>
    	    <p><a href="#" class="btn btn-primary btn-block" role="button">Button</a></p>
    	</div>
		<?php endforeach; ?>

	</div>
	<nav aria-label="Page navigation">
		<ul class="pagination">
			<li class="disabled">
				<a href="#" aria-label="Previous">
					<span aria-hidden="true">«</span>
				</a>
			</li>

			<!-- -->
			<li class="active">
				<a href="index.php?page=1">1</a>
			</li>
			<!-- -->
			<li>
				<a href="index.php?page=2">2</a>
			</li>

			<li>
				<a href="index.php?page=2" aria-label="Next">
					<span aria-hidden="true">»</span>
				</a>
			</li>
		</ul>
	</nav>
</main><!-- container -->

<?php include 'templates/footer.view.php' ?>